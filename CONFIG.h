/*
 * CONFIG.h
 *
 *  Created on: 04/04/2017
 *      Author: esteban
 */
#ifndef SOURCE_CONFIG_H_
#define SOURCE_CONFIG_H_


#include <string.h>

#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
/*#include "fsl_debug_console.h"*/

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "task.h"

/* FreeRTOS kernel includes. */
#include "event_groups.h"
#include "semphr.h"
#include "stdio.h"
#include "timers.h"

/** Handlers. */
BaseType_t xPriority = pdFALSE;

EventGroupHandle_t TERATERM_EVENTS;
QueueHandle_t TERATERM_QUEUE;

EventGroupHandle_t BLUETOOTH_EVENTS;
QueueHandle_t BLUETOOTH_QUEUE;


/** Event FLAG BITS**/

#define CHAR (1 << 0)	/** Caracter alfanumérico presionado */
#define ALL (1 << 1)	/** Total de caracteres alfanuméricos presionados */
#define ENT (1 << 2)	/** Enter presionado */
#define ESC (1 << 3)	/** Esc presionado */
#define DONE (1 << 4)	/** Menu terminado */

/**  Key Inputs**/
#define ENTER 0x0D
#define ESCAPE 0x1B



void Inicializacion(void);
void Q_E_S_Creation(void);






#endif /* SOURCE_CONFIG_H_ */
