/*
 * Copyright (c) 2013 - 2016, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * This is template for main module created by New Kinetis SDK 2.x Project Wizard. Enjoy!
 **/

#include "CONFIG.h"
#include "TASK_CONTROL.h"
#include "I2CMEM.h"
//#include "RELOJ.h"
#include "semphr.h"

/* Task priorities. */

#define INIT_MENU_PRIORITY 	(configMAX_PRIORITIES - 1)
#define Menu_task_PRIORITY 	(configMAX_PRIORITIES - 2) 	//No creo que sea necesario manejar las prioridades de esta manera
#define TASK_1_PRIORITY 	(configMAX_PRIORITIES - 2)	//Preguntar al profesor
#define TASK_2_PRIORITY 	(configMAX_PRIORITIES - 2)


int main(void) {

  /** Init board hardware. **/

  Inicializacion();
  Q_E_S_Creation();  		/** Queue Events Semaphore creation**/




  /** Create RTOS task */
  /** Menu Tasks**/
    xTaskCreate(INIT_MENU, "INIT_MENU", configMINIMAL_STACK_SIZE, NULL, INIT_MENU_PRIORITY, &INIT_MENU_HANDLE);
    xTaskCreate(MENU_SELECT,"MENU_SELECT",configMINIMAL_STACK_SIZE,(void *) UART0,Menu_task_PRIORITY,&MENU_SELECT_HANDLE);
    xTaskCreate(MENU_SELECT_BT,"MENU_SELECT_BT",configMINIMAL_STACK_SIZE,(void *) UART3,Menu_task_PRIORITY,&MENU_SELECT_BT_HANDLE);

  /** Menu selection option tasks**/
    xTaskCreate(TASK_1, "TASK_1",configMINIMAL_STACK_SIZE,NULL, TASK_1_PRIORITY, &TASK_1_HANDLER);
    xTaskCreate(TASK_2, "TASK_2",configMINIMAL_STACK_SIZE,NULL, TASK_2_PRIORITY, &TASK_2_HANDLER);
    xTaskCreate(TASK_3, "TASK_3",configMINIMAL_STACK_SIZE,NULL, TASK_2_PRIORITY, &TASK_3_HANDLER);
    xTaskCreate(TASK_4, "TASK_4",configMINIMAL_STACK_SIZE,NULL, TASK_2_PRIORITY, &TASK_4_HANDLER);
    xTaskCreate(TASK_5, "TASK_5",configMINIMAL_STACK_SIZE,NULL, TASK_2_PRIORITY, &TASK_5_HANDLER);
    xTaskCreate(TASK_6, "TASK_6",configMINIMAL_STACK_SIZE,NULL, TASK_2_PRIORITY, &TASK_6_HANDLER);
    xTaskCreate(TASK_7, "TASK_7",configMINIMAL_STACK_SIZE,NULL, TASK_2_PRIORITY, &TASK_7_HANDLER);
    xTaskCreate(TASK_8, "TASK_8",configMINIMAL_STACK_SIZE,(void *) UART0, TASK_2_PRIORITY, &TASK_8_HANDLER);
    xTaskCreate(TASK_8_BT, "TASK_8_BT",configMINIMAL_STACK_SIZE,(void *) UART3, 3, &TASK_8_HANDLER_BT);
    xTaskCreate(TASK_9, "TASK_9",configMINIMAL_STACK_SIZE,NULL, TASK_2_PRIORITY, &TASK_9_HANDLER);

  /** Tareas para la I2C**/
  //  xTaskCreate(I2C_WRITE, "I2C_WRITE",configMINIMAL_STACK_SIZE,NULL, TASK_2_PRIORITY, &I2C_WRITE_HANDLER);
  //  xTaskCreate(I2C_READ, "I2C_READ",configMINIMAL_STACK_SIZE,NULL,TASK_2_PRIORITY, &I2C_READ_HANDLER);
  /** Tareas para la RTC **/
  //  xTaskCreate(RTC_READ, "RTC_READ",configMINIMAL_STACK_SIZE,NULL, TASK_2_PRIORITY, &RTC_READ_HANDLER);
  //  xTaskCreate(RTC_WRITE, "TASK_3",configMINIMAL_STACK_SIZE,NULL, TASK_2_PRIORITY, &RTC_WRITE_HANDLER);


 //   vTaskSuspend(MENU_SELECT_HANDLE );
 //   vTaskSuspend(TASK_1_HANDLER );
 //   vTaskSuspend(TASK_2_HANDLER );
 //   vTaskSuspend(TASK_6_HANDLER);

//    vTaskSuspend(TASK_9_HANDLER);

    vTaskStartScheduler();

  for(;;) { /* Infinite loop to avoid leaving the main function */
    __asm("NOP"); /* something to use as a breakpoint stop while looping */
  }
}



