/*
 * I2CMEM.c
 *
 *  Created on: 12/04/2017
 *      Author: esteban
 */

#include <string.h>
#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "I2CMEM.h"
#include "TASK_CONTROL.h"
#include "CONFIG.h"
#include "UART.h"
#include "semphr.h"
#include "RANDF.h"

void I2C_INIT(void)
{


	/** I2C CONFIG**/
	i2c_master_config_t I2C_CONFIG;
	I2C_CONFIG.enableMaster 	 = true;
	I2C_CONFIG.enableHighDrive 	 = false;
	I2C_CONFIG.enableStopHold 	 = false;
	I2C_CONFIG.baudRate_Bps		 = 100000;  //clock frecuency taken from the 24LC256
	I2C_CONFIG.glitchFilterWidth = 0;

	/** GET I2C FRECUENCY */
	uint32_t I2C_CLK;
	I2C_CLK = CLOCK_GetFreq(I2C1_CLK_SRC);

	/** CLOCK ENABLE FOR I2C */
	CLOCK_EnableClock(kCLOCK_PortC);


	/** Set pins for I2C */
	port_pin_config_t PIN;
	PIN.pullSelect = kPORT_PullUp;
	PIN.slewRate = kPORT_FastSlewRate;
	PIN.passiveFilterEnable = kPORT_PassiveFilterDisable;
	PIN.openDrainEnable = kPORT_OpenDrainEnable;
	PIN.driveStrength = kPORT_LowDriveStrength;
	PIN.mux = kPORT_MuxAlt2;
	PIN.lockRegister = kPORT_UnlockRegister;

	PORT_SetPinConfig(PORTC, 10, &PIN);		/**SCL*/
	PORT_SetPinConfig(PORTC, 11, &PIN);		/**SDA*/

	/** SET I2C INTERRUPT PRIORITY */
	NVIC_SetPriority(I2C1_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);

	/** INITIALIZE I2C */
	status_t STATUS_I2C;
//	STATUS_I2C = I2C_RTOS_Init(&I2C_TRANSFER_HANDLE, I2C1, &I2C_CONFIG, I2C_CLK);
	STATUS_I2C = I2C_RTOS_Init(&I2C_TRANSFER_HANDLE, I2C1, &I2C_CONFIG, I2C_CLK);
//I2C_MasterInit(I2C)
	if (kStatus_Success == STATUS_I2C)
	{
		GPIO_delay(10000);
		I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
		I2C_TRANSFER.slaveAddress = 0x50;
		I2C_TRANSFER.direction = kI2C_Read;
		I2C_TRANSFER.subaddress = 0x0D;
		I2C_TRANSFER.subaddressSize = 1;
		I2C_TRANSFER.data = &i2c_mem_reg;
		I2C_TRANSFER.dataSize = 1;
		GPIO_delay(10000);
//		STATUS_I2C = I2C_MasterTransferNonBlocking(I2C1,&I2C_TRANSFER_HANDLE,&I2C_TRANSFER);
		STATUS_I2C = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
		GPIO_delay(10000);
	}
	if(STATUS_I2C != kStatus_Success) for(;;);
}

/** Tasks to write and read memory. */
void I2C_WRITE(void *pvParameters)
{
	UART_Type *base = (UART_Type *) pvParameters;

	for(;;)
	{
	I2C_INIT();

	int32_t Counter;
	status_t status;



		for (Counter = 0; Counter <= MENSAJE; Counter++)

		{
			I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
			I2C_TRANSFER.slaveAddress = SLAVE_ADDR_24LC256;
			I2C_TRANSFER.direction = kI2C_Write;
			I2C_TRANSFER.subaddress = ADDR;
			I2C_TRANSFER.subaddressSize = 2;
			I2C_TRANSFER.data = &I2C_BUFFER[Counter];
			I2C_TRANSFER.dataSize = 1;
			status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);


			vTaskDelay (10);

			/** Error si no se pudo leer */
			if (kStatus_Fail == status)
			{
				UART_WriteBlocking(base,(void *)"\033[2B",sizeof("\033[2B"));
				UART_WriteBlocking(base,(void *)"\033[50D",sizeof("\033[50D"));
				UART_WriteBlocking(base,(void *)"ERROR DE CONEXION",sizeof("ERROR DE CONEXION"));
		//		xEventGroupSetBits(xEventsMem, undone);
				break;
			}

			/** Si el dato es el �ltmo, imprimir mensaje */
			if (Counter == (MENSAJE - 1))
			{
				UART_WriteBlocking(base,(void *)"\n\rLa memoria ha sido escrita.",sizeof("La memoria ha sido escrita."));
				break;
			}

			ADDR = ADDR + 1;
		}
		//break;

			vTaskDelay(1000);

	vTaskSuspend(I2C_WRITE_HANDLER);
	}
}

void I2C_READ(void *pvParameters)
{
	int32_t Counter;
	UART_Type *base = (UART_Type *) pvParameters;

	status_t status;
	uint8_t who_am_i_reg ;
	vTaskDelay (100);
	for(;;){
	for(;;)
	{
	//	I2C_INIT();

		//MENSAJE = 4;
		for (Counter = 0; Counter <= MENSAJE; Counter++)
		{
			I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
			I2C_TRANSFER.slaveAddress = SLAVE_ADDR_24LC256;
			I2C_TRANSFER.direction = kI2C_Read;
			I2C_TRANSFER.subaddress = ADDR;
			I2C_TRANSFER.subaddressSize = 2;
			I2C_TRANSFER.data = &I2C_BUFFER[Counter];
			I2C_TRANSFER.dataSize = 1;

			status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
			vTaskDelay (10);
			/** Error si no se pudo leer */
			if (kStatus_Fail == status)
			{
				UART_WriteBlocking(base,(void *)"\033[2B",sizeof("\033[2B"));
				UART_WriteBlocking(base,(void *)"\033[50D",sizeof("\033[50D"));
				UART_WriteBlocking(base,(void *)"ERROR DE CONEXION",sizeof("ERROR DE CONEXION"));
//				xEventGroupSetBits(TERATERM_EVENTS, undone);
				break;
			}

			/** Si el dato es vac�o, sustituir por " " */
			if ((I2C_BUFFER[Counter] == 0x0)) break;
			if ((I2C_BUFFER[Counter] < 0x20) || (I2C_BUFFER[Counter] > 0x7f)) I2C_BUFFER[Counter] = 0x20;


			UART_WriteByte(UART0,I2C_BUFFER[Counter]);
			/** Si el dato es el �ltmo, imprimir mensaje */
			if (Counter == (MENSAJE - 1))
			{
				UART_WriteBlocking(base,(void *)"\033[1B",sizeof("\033[1B"));
				UART_WriteBlocking(base,(void *)"\033[100D",sizeof("\033[100D"));
				UART_WriteBlocking(base,(void *)"La memoria ha sido leida.",sizeof("La memoria ha sido leida."));
				UART_WriteBlocking(base,(void *)"\033[1B",sizeof("\033[1B"));
				UART_WriteBlocking(base,(void *)"\033[100D",sizeof("\033[100D"));
				break;
			}

			ADDR = ADDR + 1;
		}
		break;
	}
	vTaskSuspend(I2C_READ_HANDLER);
}}

