/*
 * RELOJ.c
 *
 *  Created on: 15/04/2017
 *      Author: esteban
 */

#include <string.h>
#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "I2CMEM.h"
#include "TASK_CONTROL.h"
#include "CONFIG.h"
#include "UART.h"
#include "semphr.h"
#include "RELOJ.h"
#include "RANDF.h"

//#define ANIO  	2017  /*!< Range from 1970 to 2099.*/
//#define	MES 	04  /*!< Range from 1 to 12.*/
//#define DIA  	15   /*!< Range from 1 to 31 (depending on month).*/
//#define HORA  	6   /*!< Range from 0 to 23.*/
//#define MINUTO 	28 /*!< Range from 0 to 59.*/
//#define SEGUNDO 35  /*!< Range from 0 to 59.*/
#define RTC_ADDR  (0x6FU)


void RTC_init(void)
{


	/** I2C CONFIG**/
	i2c_master_config_t I2C_CONFIG;
	I2C_CONFIG.enableMaster 	 = true;
	I2C_CONFIG.enableHighDrive 	 = false;
	I2C_CONFIG.enableStopHold 	 = false;
	I2C_CONFIG.baudRate_Bps		 = 100000;  //clock frecuency taken from the 24LC256
	I2C_CONFIG.glitchFilterWidth = 0;

	/** GET I2C FRECUENCY */
	uint32_t I2C_CLK;
	I2C_CLK = CLOCK_GetFreq(I2C1_CLK_SRC);

	/** CLOCK ENABLE FOR I2C */
	CLOCK_EnableClock(kCLOCK_PortC);

	/** Set pins for I2C */
	port_pin_config_t PIN;
	PIN.pullSelect = kPORT_PullUp;
	PIN.slewRate = kPORT_FastSlewRate;
	PIN.passiveFilterEnable = kPORT_PassiveFilterDisable;
	PIN.openDrainEnable = kPORT_OpenDrainEnable;
	PIN.driveStrength = kPORT_LowDriveStrength;
	PIN.mux = kPORT_MuxAlt2;
	PIN.lockRegister = kPORT_UnlockRegister;

	PORT_SetPinConfig(PORTC, 10, &PIN);		/**SCL*/
	PORT_SetPinConfig(PORTC, 11, &PIN);		/**SDA*/

	/** SET I2C INTERRUPT PRIORITY */
	NVIC_SetPriority(I2C1_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);

	/** INITIALIZE I2C */
	status_t STATUS_I2C;
	//STATUS_I2C = I2C_RTOS_Init(&I2C_TRANSFER_HANDLE, I2C1, &I2C_CONFIG, I2C_CLK);
	STATUS_I2C = 0;//I2C_RTOS_Init(&I2C_TRANSFER_HANDLE, I2C1, &I2C_CONFIG, I2C_CLK);

	i2c_mem_reg = 0x80;
	if (kStatus_Success == STATUS_I2C)
	{
		I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
		I2C_TRANSFER.slaveAddress = RTC_ADDR;  //0x6F
		I2C_TRANSFER.direction = kI2C_Write;
		I2C_TRANSFER.subaddress = 0x00;
		I2C_TRANSFER.subaddressSize = 1;
		I2C_TRANSFER.data = &i2c_mem_reg;		//0x80
	//	I2C_TRANSFER.dataSize = sizeof(i2c_mem_reg);
		I2C_TRANSFER.dataSize = 1;
		vTaskDelay (10);
		STATUS_I2C = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
	}
	if (kStatus_Success != STATUS_I2C) {
		for(;;);
	}

}

void RTC_READ(void *pvParameters)
{
	int32_t Counter;
	status_t status;
	uint32_t RTC_SUB_ADDR;
	uint8_t CounterD;
	uint8_t DEC,UN;
	UART_Type *base = (UART_Type *) pvParameters;
	for(;;){
	for(;;)
	{
	for (;;)
	{

		RTC_SUB_ADDR=0x00;
		for (Counter = 0;Counter<6;Counter++)
		{
			if(BANDERA_H_F==1 & Counter==3){
				RTC_SUB_ADDR++;
			}
			RTC_VAL = 0x83;
			I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
			I2C_TRANSFER.slaveAddress = RTC_ADDR;      //0x6F
			I2C_TRANSFER.direction = kI2C_Read;
			I2C_TRANSFER.subaddress = RTC_SUB_ADDR;   //0x00
			I2C_TRANSFER.subaddressSize = 1;
			I2C_TRANSFER.data = &RTC_VAL;
			I2C_TRANSFER.dataSize = 1;
			vTaskDelay (10);
			status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
			vTaskDelay (10);

			/** Error si no se pudo leer */

			if (kStatus_Fail == status)
			{
				UART_WriteBlocking(base,(void *)"\033[2B",sizeof("\033[2B"));
				UART_WriteBlocking(base,(void *)"\033[50D",sizeof("\033[50D"));
				UART_WriteBlocking(base,(void *)"ERROR DE CONEXION",sizeof("ERROR DE CONEXION"));
				break;
			}

			switch(Counter){
			case 0:
				if((BANDERA_H_F==0)){
				UART_WriteBlocking(base,(void *)"\033[5;16H",sizeof("\033[5;16H"));
				DEC = ((RTC_VAL >> 4) & 0x7);
				UN  = (RTC_VAL & 0x0F);
				UART_WriteByte(base,(DEC+0x30));
				UART_WriteByte(base,(UN+0x30));

				}
				break;
			case 1:
				if((BANDERA_H_F==0)){
				UART_WriteBlocking(base,(void *)"\033[5;13H",sizeof("\033[5;13H"));
				DEC = ((RTC_VAL >> 4) & 0x7);
				UN  = (RTC_VAL & 0x0F);
				UART_WriteByte(base,(DEC+0x30));
				UART_WriteByte(base,(UN+0x30));
				UART_WriteBlocking(base,(void *)":",sizeof(":"));
				}
				break;
			case 2:
				if((BANDERA_H_F == 0)){
					if(BANDERA_RTC == 1){
						UART_WriteBlocking(base,(void *)"\033[5;5H",sizeof("\033[5;5H"));
						if ((RTC_VAL & 0x3F) > 12){
							RTC_VAL = RTC_VAL - 0x12;
							UART_WriteBlocking(base,(void *)"PM",sizeof("PM"));
						}
						else if((RTC_VAL & 0x3F) < 12)	UART_WriteBlocking(base,(void *)"AM",sizeof("AM"));

						DEC = ((RTC_VAL >> 4) & 0x3);
						UN  = (RTC_VAL & 0x0F);
						UART_WriteBlocking(base,(void *)"\033[5;10H",sizeof("\033[5;10H"));
						UART_WriteByte(base,(DEC+0x30));
						UART_WriteByte(base,(UN+0x30));
						UART_WriteBlocking(base,(void *)":",sizeof(":"));
					}
					if(BANDERA_RTC ==2){
						UART_WriteBlocking(base,(void *)"\033[5;10H",sizeof("\033[5;10H"));
						DEC = ((RTC_VAL >> 4) & 0x3);
						UN  = (RTC_VAL & 0x0F);
						UART_WriteByte(base,(DEC+0x30));
						UART_WriteByte(base,(UN+0x30));
						UART_WriteBlocking(base,(void *)":",sizeof(":"));
					}
				}
				break;
			case 3:
				if((BANDERA_H_F == 1)){
				UART_WriteBlocking(base,(void *)"\033[5;10H",sizeof("\033[5;10H"));
				DEC = ((RTC_VAL >> 4) & 0x3);
				UN  = (RTC_VAL & 0x0F);
				UART_WriteByte(base,(DEC+0x30));
				UART_WriteByte(base,(UN+0x30));
				UART_WriteBlocking(base,(void *)":",sizeof(":"));
				}
				break;
			case 4:
				if((BANDERA_H_F == 1)){
				DEC = ((RTC_VAL >> 4) & 0x1);
				UN  = (RTC_VAL & 0x0F);
				UART_WriteByte(base,(DEC+0x30));
				UART_WriteByte(base,(UN+0x30));
				UART_WriteBlocking(base,(void *)":",sizeof(":"));
				}
				break;
			case 5:
				if((BANDERA_H_F == 1)){
				DEC = ((RTC_VAL >> 4));
				UN  = (RTC_VAL & 0x0F);
				UART_WriteByte(base,(DEC+0x30));
				UART_WriteByte(base,(UN+0x30));
				}
				break;

			}
			GPIO_delay(500000);
			RTC_SUB_ADDR++;

		}



			/** Si el dato es el �ltmo, imprimir mensaje */
			if (Counter == (MENSAJE - 1))

				break;
			}

//		RTC_SUB_ADDR = RTC_SUB_ADDR + 1;
		}
		break;
	}
	vTaskSuspend(I2C_READ_HANDLER);
}


void RTC_WRITE(void *pvParameters)
{
	int32_t Counter;
	status_t status;
	UART_Type *base = (UART_Type *) pvParameters;

	for(;;){

//		RTC_init();

	for(;;)
	{
//		RTC_SUB_ADDR = 0x00;
		for (Counter = 0;Counter<3;Counter++)
		{

			I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
			I2C_TRANSFER.slaveAddress = RTC_ADDR;      //0x6F
			I2C_TRANSFER.direction = kI2C_Write;
			I2C_TRANSFER.subaddress = RTC_SUB_ADDR;   //0x00
			I2C_TRANSFER.subaddressSize = 1;
			I2C_TRANSFER.data = &HORA_BUFFER[Counter];
			I2C_TRANSFER.dataSize = 1;
			vTaskDelay (100);
			status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
			vTaskDelay (100);

			/** Error si no se pudo leer */

			if (kStatus_Fail == status)
			{
				UART_WriteBlocking(base,(void *)"\033[2B",sizeof("\033[2B"));
				UART_WriteBlocking(base,(void *)"\033[50D",sizeof("\033[50D"));
				UART_WriteBlocking(base,(void *)"ERROR DE CONEXION",sizeof("ERROR DE CONEXION"));
				break;
			}


		RTC_SUB_ADDR = RTC_SUB_ADDR + 1;
		}
//		status_t STATUS_I2C;
//		I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
//		I2C_TRANSFER.slaveAddress = RTC_ADDR;  //0x6F
//		I2C_TRANSFER.direction = kI2C_Write;
//		I2C_TRANSFER.subaddress = 0x00;
//		I2C_TRANSFER.subaddressSize = 1;
//		I2C_TRANSFER.data = &i2c_mem_reg;		//0x80
//	//	I2C_TRANSFER.dataSize = sizeof(i2c_mem_reg);
//		I2C_TRANSFER.dataSize = 1;
//		vTaskDelay (10);
//		STATUS_I2C = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
//
//	if (kStatus_Success != STATUS_I2C) {
//		for(;;);
//	}

		break;
	}
	vTaskSuspend(RTC_WRITE_HANDLER);
}}
