/*
 * TASK_CONTROL.c
 *
 *  Created on: 05/04/2017
 *      Author: esteban
 */

#include "CONFIG.h"
#include "UART.h"
#include "TASK_CONTROL.h"
#include "RANDF.h"
#include "I2CMEM.h"
#include "RELOJ.h"
#include "LCDSPI.h"
#include "MK64F12.h"

#define TASK_2_PRIORITY 	(configMAX_PRIORITIES - 2)
//ndo#include "RELOJ.h"

/**  Variables para la interrupcion**/
uint8_t TERATERM_Char;
uint8_t BLUETOOTH_Char;
uint8_t BANDERA_RTC;
uint8_t FLAG_11[10];

/** Menu inicial**/
void INIT_MENU(void *pvParams) {

	/** Declaramos las variables que vamos a necesitar en esta tarea**/
	EventBits_t xBits;

	/****************************************************************/
	/** Escribe un mensaje inicial de inicio antes de pasar al menu**/
	/****************************************************************/

	/** Suspende todas las demas tareas **/
	vTaskSuspend(MENU_SELECT_HANDLE);
	vTaskSuspend(MENU_SELECT_BT_HANDLE);
	vTaskSuspend(TASK_1_HANDLER);
	vTaskSuspend(TASK_2_HANDLER);
	vTaskSuspend(TASK_3_HANDLER);
	vTaskSuspend(TASK_4_HANDLER);
	vTaskSuspend(TASK_5_HANDLER);
	vTaskSuspend(TASK_6_HANDLER);
	vTaskSuspend(TASK_7_HANDLER);
	vTaskSuspend(TASK_8_HANDLER);
	vTaskSuspend(TASK_8_HANDLER_BT);
	vTaskSuspend(TASK_9_HANDLER);


	I2C_INIT();
	RTC_init();

	//vTaskSuspend(xTaskClock);
	/** Limpia la pantalla*/
	CLEAR_SCREEN(UART0);
	CLEAR_SCREEN(UART3);

	/** Posiciona el cursor en la posicion x y Y especificadas**/
	//CURSOR_XY(1,5);		//La idea seria hacer una funcion que hiciera esto estilo clear screen
	UART_WriteBlocking(UART0, (void *) "\033[1;5H", sizeof("\033[1;5H"));
	UART_WriteBlocking(UART0, (void *) INIT, sizeof(INIT));

	UART_WriteBlocking(UART0, (void *) "\033[4;10H", sizeof("\033[4;10H"));
	UART_WriteBlocking(UART0, (void *) "Presione ENTER para continuar",
			sizeof("Presione ENTER para continuar"));

	UART_WriteBlocking(UART0, (void *) "\033[6;15H", sizeof("\033[5;15H"));

	/** Limpiamos las banderas para la interrupcion**/
	/** Esta es una medida de precauci�n**/
	xEventGroupClearBitsFromISR(TERATERM_EVENTS, ENT | ESC | ALL | CHAR);

	/** Esperamos un ENT en la interrupcion**/
	/** De aqu� no continua la tarea a menos que se escriba un ENT**/
	xBits = xEventGroupWaitBits(TERATERM_EVENTS, ENT, pdTRUE, pdTRUE,
	portMAX_DELAY);

	/** Se limpia la bandera ENT**/
	xEventGroupClearBitsFromISR(TERATERM_EVENTS, ENT);

	/** Se limpia el QUEUE, Esto tambien es por precauci�n**/
	xQueueReset(TERATERM_QUEUE);

	/****************************************/
	/*********** MENU PRINCIPAL *************/
	/****************************************/
	FLAG_11[9] = 3;
	/** Entra a un ciclo infinito donde va a estar el menu principal**/
	for (;;) {

		/** Suspendemos las demas tareas Excepto el contador**/
		vTaskSuspend(MENU_SELECT_HANDLE);
		vTaskSuspend(MENU_SELECT_BT_HANDLE);
		vTaskSuspend(TASK_1_HANDLER);
		vTaskSuspend(TASK_2_HANDLER);
		vTaskSuspend(TASK_3_HANDLER);
		vTaskSuspend(TASK_4_HANDLER);
		vTaskSuspend(TASK_5_HANDLER);
		vTaskSuspend(TASK_6_HANDLER);
		vTaskSuspend(TASK_7_HANDLER);
		vTaskSuspend(TASK_8_HANDLER);
		vTaskSuspend(TASK_8_HANDLER_BT);
		vTaskSuspend(TASK_9_HANDLER);

//	vTaskSuspend(xTaskClock);

		CLEAR_SCREEN(UART0);
		CLEAR_SCREEN(UART3);

		UART_WriteBlocking(UART0, (void *) "\033[1;5H", sizeof("\033[1;5H"));
		if(FLAG_11[9] == 0 | FLAG_11[9] == 3){
		/*** ESCRIBIMOS EL MENU PRINCIPAL ****/
		UART_WriteBlocking(UART0, (void *) MENU_0, sizeof(MENU_0));	//abreviado por comodidad
		UART_WriteBlocking(UART0, (void *) MENU_1, sizeof(MENU_1));
		UART_WriteBlocking(UART0, (void *) MENU_2, sizeof(MENU_2));
		UART_WriteBlocking(UART0, (void *) MENU_3, sizeof(MENU_3));
		UART_WriteBlocking(UART0, (void *) MENU_4, sizeof(MENU_4));
		UART_WriteBlocking(UART0, (void *) MENU_5, sizeof(MENU_5));
		UART_WriteBlocking(UART0, (void *) MENU_6, sizeof(MENU_6));
		UART_WriteBlocking(UART0, (void *) MENU_7, sizeof(MENU_7));
		UART_WriteBlocking(UART0, (void *) MENU_8, sizeof(MENU_8));
		UART_WriteBlocking(UART0, (void *) MENU_9, sizeof(MENU_9));
		}
		if (FLAG_11[9] == 1 | FLAG_11[9] == 3){
		/*** ESCRIBIMOS EL MENU PRINCIPAL ****/
		UART_WriteBlocking(UART3, (void *) MENU_0, sizeof(MENU_0));	//abreviado por comodidad
		UART_WriteBlocking(UART3, (void *) MENU_1, sizeof(MENU_1));
		UART_WriteBlocking(UART3, (void *) MENU_2, sizeof(MENU_2));
		UART_WriteBlocking(UART3, (void *) MENU_3, sizeof(MENU_3));
		UART_WriteBlocking(UART3, (void *) MENU_4, sizeof(MENU_4));
		UART_WriteBlocking(UART3, (void *) MENU_5, sizeof(MENU_5));
		UART_WriteBlocking(UART3, (void *) MENU_6, sizeof(MENU_6));
		UART_WriteBlocking(UART3, (void *) MENU_7, sizeof(MENU_7));
		UART_WriteBlocking(UART3, (void *) MENU_8, sizeof(MENU_8));
		UART_WriteBlocking(UART3, (void *) MENU_9, sizeof(MENU_9));
		}
		/** Indica a la tarea MENU SELECT que puede comenzar**/
		if(FLAG_11[9] == 0 | FLAG_11[9] == 3) vTaskResume(MENU_SELECT_HANDLE);
		if(FLAG_11[9] == 1 | FLAG_11[9] == 3)vTaskResume(MENU_SELECT_BT_HANDLE);

		/** Suspende Esta tarea hasta nuevo aviso**/
		vTaskSuspend(INIT_MENU_HANDLE);

	}
}

/** MENU SELECT **/
void MENU_SELECT(void *pvParams) {

	/** Declaramos las variables que vamos a necesitar en esta tarea**/
	EventBits_t xBits;			//Necesario para las interrupciones
	uint8_t ITEM_QUEUE;			//Necesario para ver los elementos en el queue
	uint8_t ERROR1;
	BANDERA_RTC = 2;
	BANDERA_H_F = 0;
	UART_Type *base = (UART_Type *) pvParams;

	for (;;) {
		/** Limpiamos las banderas por seguridad junto con el QUEUE**/
		xEventGroupClearBits(TERATERM_EVENTS, ALL | CHAR | ESC | ENT);
		xQueueReset(TERATERM_QUEUE);

		/***********************************/
		/****** SELECCION DE MENU **********/
		/***********************************/
		xBits = 0;
		/** Esperamos a que se presione algo en la terminal,**/
		/** Si se preciona ENTER o ESC debe que**/
		xBits = xEventGroupWaitBits(TERATERM_EVENTS, ESC | ENT, pdTRUE, pdFALSE,
		portMAX_DELAY);
		xEventGroupClearBits(TERATERM_EVENTS, ALL | CHAR | ESC | ENT); //limpia las banderas

		xQueueReceive(TERATERM_QUEUE, &ITEM_QUEUE, portMAX_DELAY);
		ITEM_QUEUE = ITEM_QUEUE - 0x30;

//	uint8_t ERROR1;

		//xQueuePeek( xQueue, &ERROR1, ( TickType_t ) 10 ));
		//xQueueReceive(TERATERM_QUEUE, &ERROR1, portMAX_DELAY);//al parecer se ciclan hasta que se les agrega algo al buffer

		if (xQueuePeek(TERATERM_QUEUE, &ERROR1, (TickType_t ) 10)) {
			UART_WriteBlocking(UART0, (void *) "\n\r\rMenu Invalido",
					sizeof("\n\r\rMenu Invalido"));
			UART_WriteBlocking(UART0, (void *) "\n\r\rPresione ESC para salir",
					sizeof("\n\r\rPresione ESC para salir"));
			xBits = xEventGroupWaitBits(TERATERM_EVENTS, ESC, pdTRUE, pdFALSE,
			portMAX_DELAY);
			xEventGroupClearBits(TERATERM_EVENTS, ESC); //limpia las banderas
		}

		if ( ESC != (ESC & xBits)) {
			switch (ITEM_QUEUE) {
			case MENU1:
				xQueueReset(TERATERM_QUEUE);
				UART_WriteBlocking(UART0, (void *) "\n\r\rMenu 1 seleccionado",
						sizeof("\n\r\rMenu 1 seleccionado"));
				vTaskDelay(500);
				FLAG_11[0] = 0;
				xEventGroupClearBitsFromISR(TERATERM_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_1_HANDLER);
				vTaskSuspend(MENU_SELECT_HANDLE);
				break;

			case MENU2:
				xQueueReset(TERATERM_QUEUE);
				UART_WriteBlocking(UART0, (void *) "\n\r\rMenu 2 seleccionado",
						sizeof("\n\r\rMenu 2 seleccionado"));
				vTaskDelay(500);
				FLAG_11[1] = 0;
				xEventGroupClearBitsFromISR(TERATERM_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_2_HANDLER);
				vTaskSuspend(MENU_SELECT_HANDLE);
				break;
			case MENU3:
				xQueueReset(TERATERM_QUEUE);
				UART_WriteBlocking(UART0, (void *) "\n\r\rMenu 3 seleccionado",
						sizeof("\n\r\rMenu 3 seleccionado"));
				vTaskDelay(500);
				FLAG_11[2] = 0;
				xEventGroupClearBitsFromISR(TERATERM_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_3_HANDLER);
				vTaskSuspend(MENU_SELECT_HANDLE);
				break;
			case MENU4:
				xQueueReset(TERATERM_QUEUE);
				UART_WriteBlocking(UART0, (void *) "\n\r\rMenu 4 seleccionado",
						sizeof("\n\r\rMenu 4 seleccionado"));
				vTaskDelay(500);
				FLAG_11[3] = 0;
				xEventGroupClearBitsFromISR(TERATERM_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_4_HANDLER);
				vTaskSuspend(MENU_SELECT_HANDLE);
				break;
			case MENU5:
				xQueueReset(TERATERM_QUEUE);
				UART_WriteBlocking(UART0, (void *) "\n\r\rMenu 5 seleccionado",
						sizeof("\n\r\rMenu 5 seleccionado"));
				vTaskDelay(500);
				FLAG_11[4] = 0;
				xEventGroupClearBitsFromISR(TERATERM_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_5_HANDLER);
				vTaskSuspend(MENU_SELECT_HANDLE);
				break;
			case MENU6:
				xQueueReset(TERATERM_QUEUE);
				UART_WriteBlocking(UART0, (void *) "\n\r\rMenu 6 seleccionado",
						sizeof("\n\r\rMenu 6 seleccionado"));
				vTaskDelay(500);
				FLAG_11[5] = 0;
				xEventGroupClearBitsFromISR(TERATERM_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_6_HANDLER);
				vTaskSuspend(MENU_SELECT_HANDLE);
				break;
			case MENU7:
				xQueueReset(TERATERM_QUEUE);
				UART_WriteBlocking(UART0, (void *) "\n\r\rMenu 7 seleccionado",
						sizeof("\n\r\rMenu 7 seleccionado"));
				vTaskDelay(500);
				FLAG_11[6] = 0;
				xEventGroupClearBitsFromISR(TERATERM_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_7_HANDLER);
				vTaskSuspend(MENU_SELECT_HANDLE);
				break;
			case MENU8:
				xQueueReset(BLUETOOTH_QUEUE);
				UART_WriteBlocking(base, (void *) "\n\r\rMenu 8 seleccionado",
						sizeof("\n\r\rMenu 8 seleccionado"));
				vTaskDelay(500);
				xEventGroupClearBitsFromISR(BLUETOOTH_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_8_HANDLER);
				vTaskSuspend(MENU_SELECT_HANDLE);

				break;
			case MENU9:
				xQueueReset(TERATERM_QUEUE);
				UART_WriteBlocking(UART0, (void *) "\n\r\rMenu 9 seleccionado",
						sizeof("\n\r\rMenu 9 seleccionado"));
				vTaskDelay(500);
				FLAG_11[7] = 0;
				xEventGroupClearBitsFromISR(TERATERM_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_9_HANDLER);
				vTaskSuspend(MENU_SELECT_HANDLE);
				break;
			default:
				UART_WriteBlocking(UART0, (void *) "\n\r\rMenu Invalido",
						sizeof("\n\r\rMenu Invalido"));
				break;
			}
		} else {
			vTaskResume(INIT_MENU_HANDLE);
		}

	}
}
void MENU_SELECT_BT(void *pvParams) {

	/** Declaramos las variables que vamos a necesitar en esta tarea**/
	EventBits_t xBits;			//Necesario para las interrupciones
	uint8_t ITEM_QUEUE;			//Necesario para ver los elementos en el queue
	uint8_t ERROR1;
	BANDERA_RTC = 2;
	BANDERA_H_F = 0;
	UART_Type *base = (UART_Type *) pvParams;

	for (;;) {
		/** Limpiamos las banderas por seguridad junto con el QUEUE**/
		xEventGroupClearBits(BLUETOOTH_EVENTS, ALL | CHAR | ESC | ENT);
		xQueueReset(BLUETOOTH_QUEUE);

		/***********************************/
		/****** SELECCION DE MENU **********/
		/***********************************/
		xBits = 0;
		/** Esperamos a que se presione algo en la terminal,**/
		/** Si se preciona ENTER o ESC debe que**/
		xBits = xEventGroupWaitBits(BLUETOOTH_EVENTS, ESC | ENT | CHAR, pdTRUE,
		pdFALSE, portMAX_DELAY);
		xEventGroupClearBits(BLUETOOTH_EVENTS, ALL | CHAR | ESC | ENT); //limpia las banderas

		xQueueReceive(BLUETOOTH_QUEUE, &ITEM_QUEUE, portMAX_DELAY);
		ITEM_QUEUE = ITEM_QUEUE - 0x30;

//	uint8_t ERROR1;

		//xQueuePeek( xQueue, &ERROR1, ( TickType_t ) 10 ));
		//xQueueReceive(TERATERM_QUEUE, &ERROR1, portMAX_DELAY);//al parecer se ciclan hasta que se les agrega algo al buffer

		if (xQueuePeek(TERATERM_QUEUE, &ERROR1, (TickType_t ) 10)) {
			UART_WriteBlocking(base, (void *) "\n\r\rMenu Invalido",
					sizeof("\n\r\rMenu Invalido"));
			UART_WriteBlocking(base, (void *) "\n\r\rPresione ESC para salir",
					sizeof("\n\r\rPresione ESC para salir"));
			xBits = xEventGroupWaitBits(BLUETOOTH_EVENTS, ESC, pdTRUE, pdFALSE,
			portMAX_DELAY);
			xEventGroupClearBits(BLUETOOTH_EVENTS, ESC); //limpia las banderas
		}

		if ( ESC != (ESC & xBits)) {
			switch (ITEM_QUEUE) {
			case MENU1:
				xQueueReset(BLUETOOTH_QUEUE);
				UART_WriteBlocking(base, (void *) "\n\r\rMenu 1 seleccionado",
						sizeof("\n\r\rMenu 1 seleccionado"));
				vTaskDelay(500);
				FLAG_11[0] = 1;
				xEventGroupClearBitsFromISR(BLUETOOTH_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_1_HANDLER);
				vTaskSuspend(MENU_SELECT_BT_HANDLE);
				break;

			case MENU2:
				xQueueReset(BLUETOOTH_QUEUE);
				UART_WriteBlocking(base, (void *) "\n\r\rMenu 2 seleccionado",
						sizeof("\n\r\rMenu 2 seleccionado"));
				vTaskDelay(500);
				FLAG_11[1] = 1;
				xEventGroupClearBitsFromISR(BLUETOOTH_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_2_HANDLER);
				vTaskSuspend(MENU_SELECT_BT_HANDLE);
				break;
			case MENU3:
				xQueueReset(BLUETOOTH_QUEUE);
				UART_WriteBlocking(base, (void *) "\n\r\rMenu 3 seleccionado",
						sizeof("\n\r\rMenu 3 seleccionado"));
				vTaskDelay(500);
				FLAG_11[2] = 1;
				xEventGroupClearBitsFromISR(BLUETOOTH_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_3_HANDLER);
				vTaskSuspend(MENU_SELECT_BT_HANDLE);
				break;
			case MENU4:
				xQueueReset(BLUETOOTH_QUEUE);
				UART_WriteBlocking(base, (void *) "\n\r\rMenu 4 seleccionado",
						sizeof("\n\r\rMenu 4 seleccionado"));
				vTaskDelay(500);
				FLAG_11[3] = 1;
				xEventGroupClearBitsFromISR(BLUETOOTH_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_4_HANDLER);
				vTaskSuspend(MENU_SELECT_BT_HANDLE);
				break;
			case MENU5:
				xQueueReset(BLUETOOTH_QUEUE);
				UART_WriteBlocking(base, (void *) "\n\r\rMenu 5 seleccionado",
						sizeof("\n\r\rMenu 5 seleccionado"));
				vTaskDelay(500);
				FLAG_11[4] = 1;
				xEventGroupClearBitsFromISR(BLUETOOTH_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_5_HANDLER);
				vTaskSuspend(MENU_SELECT_BT_HANDLE);
				break;
			case MENU6:
				xQueueReset(BLUETOOTH_QUEUE);
				UART_WriteBlocking(base, (void *) "\n\r\rMenu 6 seleccionado",
						sizeof("\n\r\rMenu 6 seleccionado"));
				vTaskDelay(500);
				FLAG_11[5] = 1;
				xEventGroupClearBitsFromISR(BLUETOOTH_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_6_HANDLER);
				vTaskSuspend(MENU_SELECT_BT_HANDLE);
				break;
			case MENU7:
				xQueueReset(BLUETOOTH_QUEUE);
				UART_WriteBlocking(base, (void *) "\n\r\rMenu 7 seleccionado",
						sizeof("\n\r\rMenu 7 seleccionado"));
				vTaskDelay(500);
				FLAG_11[6] = 1;
				xEventGroupClearBitsFromISR(BLUETOOTH_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_7_HANDLER);
				vTaskSuspend(MENU_SELECT_BT_HANDLE);
				break;
			case MENU8:
				xQueueReset(BLUETOOTH_QUEUE);
				UART_WriteBlocking(base, (void *) "\n\r\rMenu 8 seleccionado",
						sizeof("\n\r\rMenu 8 seleccionado"));
				vTaskDelay(500);
				xEventGroupClearBitsFromISR(BLUETOOTH_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_8_HANDLER_BT);
				vTaskSuspend(MENU_SELECT_BT_HANDLE);

				break;
			case MENU9:
				xQueueReset(TERATERM_QUEUE);
				UART_WriteBlocking(base, (void *) "\n\r\rMenu 9 seleccionado",
						sizeof("\n\r\rMenu 9 seleccionado"));
				vTaskDelay(500);
				FLAG_11[7] = 1;
				xEventGroupClearBitsFromISR(TERATERM_EVENTS,
				ALL | CHAR | ENT | ESC);
				vTaskResume(TASK_9_HANDLER);
				vTaskSuspend(MENU_SELECT_BT_HANDLE);
				break;
			default:
				UART_WriteBlocking(UART0, (void *) "\n\r\rMenu Invalido",
						sizeof("\n\r\rMenu Invalido"));
				break;
			}
		} else {
			vTaskResume(INIT_MENU_HANDLE);
		}

	}
}

void TASK_1(void * pvParameters) {
	/** Declaramos las variables que vamos a necesitar en esta tarea**/
	uint16_t Counter;
	uint8_t ITEM_QUEUE;
	EventBits_t xBits;
	UART_Type *base;

	EventGroupHandle_t *EVENTS;
	QueueHandle_t *QUEUE;

	if (FLAG_11[0] == 0){
		base = UART0;
		EVENTS = TERATERM_EVENTS;
		QUEUE = TERATERM_QUEUE;
		FLAG_11[9] = 0;
	}
	else if (FLAG_11[1] == 1){
		base = UART3;
		EVENTS = BLUETOOTH_EVENTS;
		QUEUE = BLUETOOTH_QUEUE;
		FLAG_11[9] = 1;

	}
	for (;;) {
		for (;;) {
			ADDR = 0;

			/** Indicamos que leeremos un mensaje de la siguiente direccion de memoria**/
			CLEAR_SCREEN(base);
			UART_WriteBlocking(base, (void *) "\033[1;10H",
					sizeof("\033[1;10H"));
			UART_WriteBlocking(base, (void *) MEM_R, sizeof(MEM_R));

			UART_WriteBlocking(base, (void *) "\033[3;0H", sizeof("\033[3;0H"));
			UART_WriteBlocking(base, (void *) READ_DIR, sizeof(READ_DIR));

			/** Limpiamos por si acaso **/
			xEventGroupClearBits(EVENTS,
			ALL | CHAR | ESC | ENT | DONE);

			/** Escribimos la direccion de la memoria a leer*/
			for (Counter = 0; Counter < 4; Counter++) {
				xBits = xEventGroupWaitBits(EVENTS, ESC | ENT | CHAR,
				pdTRUE, pdFALSE, portMAX_DELAY);
				if (ESC == (ESC & xBits))
					break;
				else if (ENT == (ENT & xBits))
					break;
			}

			/************************************************/
			/** CORRECCION DE ERRORES AL ESCRIBIR LA MEMORIA*/
			/************************************************/
			/** Si hay error Volvemos al menu principal**/
			if (ESC == (ESC & xBits)) {
				xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
				vTaskResume(INIT_MENU_HANDLE);
				break;
			}

			/** Limpiamos**/
			xEventGroupClearBits(EVENTS,
			ALL | CHAR | ESC | ENT | DONE);

			/** Una ves ingresado los 4 digitos de la Address esperamos un enter */
			xBits = xEventGroupWaitBits(EVENTS, ESC | ENT, pdTRUE,
			pdFALSE, portMAX_DELAY);

			/** Si hay error Volvemos al menu principal**/
			if (ESC == (ESC & xEventGroupGetBits(EVENTS))) {
				xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
				vTaskResume(INIT_MENU_HANDLE);
				break;
			}

			/** Marca error si la direccion esta incompleta */
			if (uxQueueMessagesWaiting(QUEUE) != 4) {
				UART_WriteBlocking(base, (void *) "\033[5;5H",
						sizeof("\033[5;5H"));
				UART_WriteBlocking(base,
						(void *) "ERROR: direccion incompleta, presione ESC",
						sizeof("ERROR: direccion incompleta, presione ESC"));
				/** Si hay error Volvemos al menu principal**/
				if (ESC == (ESC & xEventGroupGetBits(EVENTS))) {
					xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
					vTaskResume(INIT_MENU_HANDLE);
					break;
				}
			}
			/**********************************/
			/** Calcula la direccion igual que cuando se escribio en la memoria (Esa parte se hizo primero) */
			for (Counter = 0; Counter < 4; Counter++) {
				xQueueReceive(QUEUE, &ITEM_QUEUE, portMAX_DELAY);
				ADDR = ADDR | ((ITEM_QUEUE - 0x30) << (12 - (4 * Counter)));
			}

			UART_WriteBlocking(base, (void *) "\033[4;0H",
					sizeof("\033[4;0H"));
			UART_WriteBlocking(base, (void *) "Longitud (bytes): ",
					sizeof("Longitud (bytes): "));

			/** Espera a que se escriba que tan largo va a ser el MENSAJE EN BYTES*/
			for (Counter = 0; Counter < 2; Counter++) {
				xBits = xEventGroupWaitBits(EVENTS, ESC | ENT | CHAR,
				pdTRUE, pdFALSE, portMAX_DELAY);
				xQueueReceive(QUEUE, &ITEM_QUEUE, portMAX_DELAY);
				MENSAJE = MENSAJE
						| ((ITEM_QUEUE - 0x30) << (4 - (4 * Counter)));
				if (ESC == (ESC & xBits))
					break;
				else if (ENT == (ENT & xBits))
					break;

			}
			/**************************************************/
			/** CORRECCION DE ERRORES AL ESCRIBIR LA dIRECCION*/
			/**************************************************/
			/** Si hay error Volvemos al menu principal**/
			if (ESC == (ESC & xEventGroupGetBits(EVENTS))) {
				xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
				vTaskResume(INIT_MENU_HANDLE);
				break;
			}

			/** Limpiamos las banderas**/
			xEventGroupClearBits(EVENTS,
			ALL | CHAR | ESC | ENT | DONE);
			/** Esperamos un enter despues de ingresar la longitud en bytes**/
			xBits = xEventGroupWaitBits(EVENTS, ESC | ENT | CHAR,
			pdTRUE, pdFALSE, portMAX_DELAY);

			/** Checa si la direccion esta incompleta y espera a ESC para salir**/
			if (Counter < 1 || (ESC == (xBits & ESC))) {
				UART_WriteBlocking(base, (void *) "\033[8;5H",
						sizeof("\033[8;5H"));
				UART_WriteBlocking(base, (void *) DIR_ERROR,
						sizeof(DIR_ERROR));
				break;
			}

			/** Limpiamos las banderas**/
			xEventGroupClearBits(EVENTS,
			ALL | CHAR | ESC | ENT | DONE);

			/** Error si la longitud est� incompleta */
			if ( CHAR == (xBits & CHAR)) {
//		if (uxQueueMessagesWaiting(TERATERM_QUEUE) != 2)

				UART_WriteBlocking(base, (void *) "\033[6;5H",
						sizeof("\033[6;5H"));
				UART_WriteBlocking(base, (void *) LONG_ERROR,
						sizeof(LONG_ERROR));
			}

			UART_WriteBlocking(base, (void *) "\033[2B", sizeof("\033[2B"));
			UART_WriteBlocking(base, (void *) "\033[100D",
					sizeof("\033[100D"));
			UART_WriteBlocking(base, (void *) "Contenido: ",
					sizeof("Contenido: "));
//			vTaskResume(I2C_READ_HANDLER);
		    xTaskCreate(I2C_READ, "I2C_READ",configMINIMAL_STACK_SIZE,base,TASK_2_PRIORITY, &I2C_READ_HANDLER);
			xBits = xEventGroupWaitBits(EVENTS, ESC | ENT | CHAR,
			pdTRUE, pdFALSE, portMAX_DELAY);
		    vTaskDelete(I2C_READ_HANDLER);

		    vTaskResume(INIT_MENU_HANDLE);

		}
	}
}
void TASK_2(void * pvParameters) {

	/** Declaramos las variables que vamos a necesitar en esta tarea**/
	uint8_t ITEM_QUEUE;
	uint8_t Counter = 0;
	EventBits_t xBits;
	UART_Type *base;

	EventGroupHandle_t *EVENTS;
	QueueHandle_t *QUEUE;

	if (FLAG_11[1] == 0){
		base = UART0;
		EVENTS = TERATERM_EVENTS;
		QUEUE = TERATERM_QUEUE;
		FLAG_11[9] = 0;
	}
	else if (FLAG_11[1] == 1){
		base = UART3;
		EVENTS = BLUETOOTH_EVENTS;
		QUEUE = BLUETOOTH_QUEUE;
		FLAG_11[9] = 1;

	}

	for (;;) {
		for (;;) {

			/** Reseteamos la address  y la longitud del mensaje junto con el counter**/
			ADDR = 0;
			MENSAJE = 0;
			Counter = 0;

			/** Escribimos en pantalla**/
			CLEAR_SCREEN(base);
			UART_WriteBlocking(base, (void *) "\033[1;10H",
					sizeof("\033[1;10H"));
			UART_WriteBlocking(base, (void *) ESCRIBIR, sizeof(ESCRIBIR));
			UART_WriteBlocking(base, (void *) "\033[3;0H",
					sizeof("\033[3;0H"));
			UART_WriteBlocking(base, (void *) ADDRES, sizeof(ADDRES));

			xEventGroupClearBits(EVENTS,
			ALL | CHAR | ESC | ENT | DONE);

			/** Tarea bloqueada, en espera de 4 caracteres */
			for (Counter = 0; Counter < 4; Counter++) {
				xBits = xEventGroupWaitBits(EVENTS, ESC | ENT | CHAR,
				pdTRUE, pdFALSE, portMAX_DELAY);
				if (ESC == (xBits & ESC))
					break;
				else if (ENT == (xBits & ENT))
					break;
			}

			/****************************/
			/*****ERROR CHECK************/

			/** Si hay error Volvemos al menu principal**/
			if (ESC == (xBits & ESC)) {
				UART_WriteBlocking(base, (void *) "\033[8;5H",
						sizeof("\033[8;5H"));
				UART_WriteBlocking(base,
						(void *) "ERROR: direccion incompleta, presione ESC",
						sizeof("ERROR: direccion incompleta, presione ESC"));
				xBits = xEventGroupWaitBits(EVENTS, ESC | ENT | CHAR,
				pdTRUE, pdFALSE, portMAX_DELAY);
				xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
				vTaskResume(INIT_MENU_HANDLE);
				break;
			}

			/** Limpiamos y esperamos el enter**/
			xEventGroupClearBits(EVENTS,
			ALL | CHAR | ESC | ENT | DONE);
			xBits = xEventGroupWaitBits(EVENTS, ESC | ENT | CHAR,
			pdTRUE, pdFALSE, portMAX_DELAY);

			/** Checamos que si sean 4**/
			if (Counter < 4 || (ESC == (xBits & ESC))) {
				CLEAR_SCREEN(base);
				UART_WriteBlocking(base, (void *) "\033[8;5H",
						sizeof("\033[8;5H"));
				UART_WriteBlocking(base,
						(void *) "ERROR: direccion incompleta, presione ESC",
						sizeof("ERROR: direccion incompleta, presione ESC"));
				break;
			}

			/****************************/
			/** C�lculamos la direcci�n */
			for (Counter = 0; Counter < 4; Counter++) {
				xQueueReceive(QUEUE, &ITEM_QUEUE, portMAX_DELAY); //Recivimos del queue lo que tiene actualmente y lo ponemos en item queue
				ADDR = ADDR | ((ITEM_QUEUE - 0x30) << (12 - (4 * Counter))); //ingeniosa manera de recorrer de manera hexadecimal
			}

			/** Checamos que si se pasaran bien los 4 digitos del address**/
			if (ADDR < 4 || (ESC == (xBits & ESC))) {
				CLEAR_SCREEN(base);
				UART_WriteBlocking(base, (void *) "\033[8;5H",
						sizeof("\033[8;5H"));
				UART_WriteBlocking(base,
						(void *) "ERROR: direccion incompleta, presione ESC",
						sizeof("ERROR: direccion incompleta, presione ESC"));
				break;
			}

			/** Indicamos que vamos a guardar un texto**/
			UART_WriteBlocking(base, (void *) "\033[7;0H",
					sizeof("\033[7;0H"));
			UART_WriteBlocking(base, (void *) "Texto a guardar: ",
					sizeof("Texto a guardar: "));
			/** Reset del queue por si acaso**/
			xQueueReset(QUEUE);

			/** Escribimos el mensaje que queremos escribir**/
			for (Counter = 0; Counter < 150; Counter++) {
				xBits = xEventGroupWaitBits(EVENTS, ESC | ENT | CHAR,
				pdTRUE, pdFALSE, portMAX_DELAY);
				if (ESC == (ESC & xBits))
					break;
				else if (ENT == (ENT & xBits))
					break;
			}
			/** Limpiamos el Enter o el ESZ que usamos para salir de la escritura del mensaje**/
			xEventGroupClearBits(EVENTS,
			ALL | CHAR | ESC | ENT | DONE);

			/** C�lculamos la longuitud del mensaje */
			MENSAJE = uxQueueMessagesWaiting(QUEUE);

			/** Guardamos lo que se escribio en un buffer que luego mandaremos a escribir en la memoria */
			for (Counter = 0; Counter < MENSAJE; Counter++) {
				xQueueReceive(QUEUE, &ITEM_QUEUE, portMAX_DELAY);
				I2C_BUFFER[Counter] = ITEM_QUEUE;
			}
			/** Mandamos escribir lo que hay actualmente en el I2C_BUFFER **/
			//vTaskResume(I2C_WRITE_HANDLER);
		    xTaskCreate(I2C_WRITE, "I2C_WRITE",configMINIMAL_STACK_SIZE,base, TASK_2_PRIORITY, &I2C_WRITE_HANDLER);

			/** Esperamos a que se ingrese un ESC o un ENT**/
			xBits = xEventGroupWaitBits(EVENTS, ESC | ENT, pdTRUE,
			pdFALSE, portMAX_DELAY);
			vTaskDelete(I2C_WRITE_HANDLER);
			xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
			/** Volvemos al menu principal**/
			vTaskResume(INIT_MENU_HANDLE);

		}
	}
}
void TASK_3(void * pvParameters) {

	/** Declaramos las variables que vamos a necesitar en esta tarea**/
	uint8_t ITEM_QUEUE;
	uint8_t Counter = 0;
	EventBits_t xBits;
	UART_Type *base;

	EventGroupHandle_t *EVENTS;
	QueueHandle_t *QUEUE;

	if (FLAG_11[2] == 0){
		base = UART0;
		EVENTS = TERATERM_EVENTS;
		QUEUE = TERATERM_QUEUE;
		FLAG_11[9] = 0;
	}
	else if (FLAG_11[2] == 1){
		base = UART3;
		EVENTS = BLUETOOTH_EVENTS;
		QUEUE = BLUETOOTH_QUEUE;
		FLAG_11[9] = 1;
	}

	for (;;) {
		for (;;) {
			CLEAR_SCREEN(base);
			UART_WriteBlocking(base, (void *) "\033[1;10H",
					sizeof("\033[1;10H"));
			UART_WriteBlocking(base, (void *) GET_HOUR, sizeof(GET_HOUR));

			/** limpiar banderas para utilizar **/
			xEventGroupClearBits(EVENTS,
			ALL | CHAR | ESC | ENT | DONE);
			/** Tarea bloqueada, en espera de 4 caracteres */
			for (Counter = 0; Counter < 6; Counter++) {
				xBits = xEventGroupWaitBits(EVENTS, ESC | CHAR, pdTRUE,
				pdFALSE, portMAX_DELAY);

				if (ESC == (xBits & ESC))
					break;
				else if (ENT == (xBits & ENT))
					break;
				if ((Counter == 1) | (Counter == 3))
					UART_WriteBlocking(base, (void *) ":", sizeof(":"));
			}

			/** Recibe los datos del queue y checa por errores **/
			for (Counter = 0; Counter < 6; Counter++) {
				xQueueReceive(QUEUE, &ITEM_QUEUE, portMAX_DELAY);
				HORA_BUFFER[Counter] = ITEM_QUEUE - 0x30;

				/** Checar Esc o error**/
				//xQueueReceive(TERATERM_QUEUE, &ITEM_QUEUE, portMAX_DELAY);
				if ((ESC == (xBits & ESC)) | (ITEM_QUEUE >= 0x40)
						| (ITEM_QUEUE < 0x30)) {
					UART_WriteBlocking(base, (void *) "\033[8;5H",
							sizeof("\033[8;5H"));
					UART_WriteBlocking(base,
							(void *) "ERROR: Hora incorrecta, presione ESC",
							sizeof("ERROR: Hora incorrecta, presione ESC"));
					xBits = xEventGroupWaitBits(EVENTS, ESC, pdTRUE,
					pdFALSE, portMAX_DELAY);
					xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
					vTaskResume(INIT_MENU_HANDLE);
					break;
				}
			}

			/** ACOMODA LOS DATOS PARA ESCRIBIRLOS **/
			HORA = (((HORA_BUFFER[0] << 4) & 0x30) + (HORA_BUFFER[1]));
			MINUTO = (((HORA_BUFFER[2] << 4) & 0x70) + (HORA_BUFFER[3]));
			SEGUNDO = (((HORA_BUFFER[4] << 4) & 0x70) + (HORA_BUFFER[5]));
			HORA_BUFFER[0] = SEGUNDO + 0x80;
			HORA_BUFFER[1] = MINUTO;
			HORA_BUFFER[2] = HORA;

			/** Mandamos escribir la hora **/
			//vTaskResume(RTC_WRITE_HANDLER);
			xTaskCreate(RTC_WRITE, "TASK_3",configMINIMAL_STACK_SIZE,base, TASK_2_PRIORITY, &RTC_WRITE_HANDLER);

			UART_WriteBlocking(base, (void *) "\033[3;0H",
					sizeof("\033[3;0H"));
			UART_WriteBlocking(base, (void *) GET_CONFIRM,
					sizeof(GET_CONFIRM));

			xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
			/** Esperamos a que se ingrese un ESC o un ENT**/
			xBits = xEventGroupWaitBits(EVENTS, ESC | ENT, pdTRUE,
			pdFALSE, portMAX_DELAY);
			vTaskDelete(RTC_WRITE_HANDLER);
			xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
			/** Volvemos al menu principal**/
			vTaskResume(INIT_MENU_HANDLE);
		}
	}
}
void TASK_4(void * pvParameters) {

	/** Declaramos las variables que vamos a necesitar en esta tarea**/
	uint8_t ITEM_QUEUE;
	uint8_t Counter = 0;
	EventBits_t xBits;
	UART_Type *base;

	EventGroupHandle_t *EVENTS;
	QueueHandle_t *QUEUE;

	if (FLAG_11[3] == 0){
		base = UART0;
		EVENTS = TERATERM_EVENTS;
		QUEUE = TERATERM_QUEUE;
		FLAG_11[9] = 0;

	}
	else if (FLAG_11[3] == 1){
		base = UART3;
		EVENTS = BLUETOOTH_EVENTS;
		QUEUE = BLUETOOTH_QUEUE;
		FLAG_11[9] = 1;

	}

	for (;;) {
		for (;;) {
			CLEAR_SCREEN(base);
			UART_WriteBlocking(base, (void *) "\033[1;10H",
					sizeof("\033[1;10H"));
			UART_WriteBlocking(base, (void *) GET_DATE, sizeof(GET_DATE));

			/** limpiar banderas para utilizar **/
			xEventGroupClearBits(EVENTS,
			ALL | CHAR | ESC | ENT | DONE);
			/** Tarea bloqueada, en espera de 4 caracteres */
			for (Counter = 0; Counter < 6; Counter++) {
				xBits = xEventGroupWaitBits(EVENTS, ESC | CHAR, pdTRUE,
				pdFALSE, portMAX_DELAY);

				if (ESC == (xBits & ESC))
					break;
				else if (ENT == (xBits & ENT))
					break;
				if ((Counter == 1) | (Counter == 3) | (Counter == 5))
					UART_WriteBlocking(base, (void *) ":", sizeof(":"));
			}

			/** Recibe los datos del queue y checa por errores **/
			for (Counter = 0; Counter < 6; Counter++) {
				xQueueReceive(QUEUE, &ITEM_QUEUE, portMAX_DELAY);
				HORA_BUFFER[Counter] = ITEM_QUEUE - 0x30;

				/** Checar Esc o error**/
				//xQueueReceive(TERATERM_QUEUE, &ITEM_QUEUE, portMAX_DELAY);
				if ((ESC == (xBits & ESC)) | (ITEM_QUEUE >= 0x40)
						| (ITEM_QUEUE < 0x30)) {
					UART_WriteBlocking(base, (void *) "\033[8;5H",
							sizeof("\033[8;5H"));
					UART_WriteBlocking(base,
							(void *) "ERROR: Fecha incorrecta, presione ESC",
							sizeof("ERROR: Fecha incorrecta, presione ESC"));
					xBits = xEventGroupWaitBits(EVENTS, ESC, pdTRUE,
					pdFALSE, portMAX_DELAY);
					xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
					vTaskResume(INIT_MENU_HANDLE);
					break;
				}
			}

			/** ACOMODA LOS DATOS PARA ESCRIBIRLOS **/
			DIA = (((HORA_BUFFER[0] << 4) & 0x30) + (HORA_BUFFER[1]));
			MES = (((HORA_BUFFER[2] << 4) & 0x30) + (HORA_BUFFER[3]));
			ANIO = (((HORA_BUFFER[4] << 4)) + (HORA_BUFFER[5]));
			HORA_BUFFER[0] = DIA;
			HORA_BUFFER[1] = MES;
			HORA_BUFFER[2] = ANIO;

			/** Mandamos escribir la hora **/
			RTC_SUB_ADDR = 0x04;

//			vTaskResume(RTC_WRITE_HANDLER);
			xTaskCreate(RTC_WRITE, "TASK_3",configMINIMAL_STACK_SIZE,base, TASK_2_PRIORITY, &RTC_WRITE_HANDLER);

			UART_WriteBlocking(base, (void *) "\033[3;0H",
					sizeof("\033[3;0H"));
			UART_WriteBlocking(base, (void *) DATE_SUCCESS,
					sizeof(DATE_SUCCESS));

			xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
			/** Esperamos a que se ingrese un ESC o un ENT**/
			xBits = xEventGroupWaitBits(EVENTS, ESC | ENT, pdTRUE,
			pdFALSE, portMAX_DELAY);
			vTaskDelete(RTC_WRITE_HANDLER);
			xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
			/** Volvemos al menu principal**/
			vTaskResume(INIT_MENU_HANDLE);
		}
	}
}
void TASK_5(void * pvParameters) {
	EventBits_t xBits;
	status_t status;
	uint8_t ITEM_QUEUE;
	UART_Type *base;

		EventGroupHandle_t *EVENTS;
		QueueHandle_t *QUEUE;

		if (FLAG_11[4] == 0){
			base = UART0;
			EVENTS = TERATERM_EVENTS;
			QUEUE = TERATERM_QUEUE;
			FLAG_11[9] = 0;
		}
		else if (FLAG_11[4] == 1){
			base = UART3;
			EVENTS = BLUETOOTH_EVENTS;
			QUEUE = BLUETOOTH_QUEUE;
			FLAG_11[9] = 1;
		}


	for (;;) {
		CLEAR_SCREEN(base);

		//		uint8_t FORMAT_SUCC[100] 	  = "\n\r Formato escrito correctamente";
		//		uint8_t FORMAT_FAIL[100] 	  = "\n\r ERROR: Seleccion incorrecta de formato";

		UART_WriteBlocking(base, (void *) "\033[1;10H", sizeof("\033[1;10H"));
		UART_WriteBlocking(base, (void *) GET_FORMAT, sizeof(GET_FORMAT));

//			RTC_SUB_ADDR = 0x02; //Comienza con las horas

		xBits = xEventGroupWaitBits(EVENTS, ESC | CHAR, pdTRUE,
		pdFALSE, portMAX_DELAY);
		xQueueReceive(QUEUE, &ITEM_QUEUE, portMAX_DELAY);
		ITEM_QUEUE = ITEM_QUEUE - 0x30;

		BANDERA_RTC = 2;
		RTC_SUB_ADDR = 0x02;
		uint8_t x = 0x00;
		I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
		I2C_TRANSFER.slaveAddress = RTC_ADDR;      //0x6F
		I2C_TRANSFER.direction = kI2C_Read;
		I2C_TRANSFER.subaddress = RTC_SUB_ADDR;   //0x00
		I2C_TRANSFER.subaddressSize = 1;
		I2C_TRANSFER.data = &x;
		I2C_TRANSFER.dataSize = 1;
		vTaskDelay(10);
		status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
		vTaskDelay(10);

		if (ITEM_QUEUE == 1) {
			BANDERA_RTC = 1;
			RTC_SUB_ADDR = 0x02;

			x = x | 0x40;

			I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
			I2C_TRANSFER.slaveAddress = RTC_ADDR;      //0x6F
			I2C_TRANSFER.direction = kI2C_Write;
			I2C_TRANSFER.subaddress = RTC_SUB_ADDR;   //0x00
			I2C_TRANSFER.subaddressSize = 1;
			I2C_TRANSFER.data = &x;
			I2C_TRANSFER.dataSize = 1;
			vTaskDelay(10);
			status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
			vTaskDelay(10);
		} else if (ITEM_QUEUE == 2) {
			BANDERA_RTC = 2;
			RTC_SUB_ADDR = 0x02;
			x = x & 0xBF;
			I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
			I2C_TRANSFER.slaveAddress = RTC_ADDR;      //0x6F
			I2C_TRANSFER.direction = kI2C_Write;
			I2C_TRANSFER.subaddress = RTC_SUB_ADDR;   //0x00
			I2C_TRANSFER.subaddressSize = 1;
			I2C_TRANSFER.data = &x;
			I2C_TRANSFER.dataSize = 1;
			vTaskDelay(10);
			status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
			vTaskDelay(10);

		} else if (ESC == (xBits & ESC))
			break;
		else if (ENT == (xBits & ENT))
			break;
		else if ((ITEM_QUEUE != 1) & (ITEM_QUEUE != 2))
			break;

		BANDERA_H_F = 0;

		//vTaskResume(RTC_READ_HANDLER);
		xTaskCreate(RTC_READ, "RTC_READ",configMINIMAL_STACK_SIZE,base, TASK_2_PRIORITY, &RTC_READ_HANDLER);

		xBits = xEventGroupWaitBits(EVENTS, ESC | ENT, pdTRUE, pdFALSE,
		portMAX_DELAY);
		xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
		vTaskDelete(RTC_READ_HANDLER);
		vTaskResume(INIT_MENU_HANDLE);
	}

}
void TASK_6(void * pvParameters) {
	EventBits_t xBits;
	UART_Type *base;
	EventGroupHandle_t *EVENTS;
	QueueHandle_t *QUEUE;

	if (FLAG_11[5] == 0){
		base = UART0;
		EVENTS = TERATERM_EVENTS;
		QUEUE = TERATERM_QUEUE;
		FLAG_11[9] = 0;
	}
	else if (FLAG_11[5] == 1){
		base = UART3;
		EVENTS = BLUETOOTH_EVENTS;
		QUEUE = BLUETOOTH_QUEUE;
		FLAG_11[9] = 1;
	}



	for (;;) {
		CLEAR_SCREEN(base);
		UART_WriteBlocking(base, (void *) "\033[1;10H", sizeof("\033[1;10H"));
		UART_WriteBlocking(base, (void *) GET_HORA, sizeof(GET_HORA));

		RTC_SUB_ADDR = 0x00; //Comienza con las horas
		BANDERA_H_F = 0;

		//BANDERA_H_F = 1;

		//vTaskResume(RTC_READ_HANDLER);
		xTaskCreate(RTC_READ, "RTC_READ",configMINIMAL_STACK_SIZE,base, TASK_2_PRIORITY, &RTC_READ_HANDLER);

		xBits = xEventGroupWaitBits(EVENTS, ESC | ENT, pdTRUE, pdFALSE,
		portMAX_DELAY);
		xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
		vTaskDelete(RTC_READ_HANDLER);
		vTaskResume(INIT_MENU_HANDLE);
	}
}
void TASK_7(void * pvParameters) {
	EventBits_t xBits;
	UART_Type *base;
	EventGroupHandle_t *EVENTS;
	QueueHandle_t *QUEUE;

	if (FLAG_11[6] == 0){
		base = UART0;
		EVENTS = TERATERM_EVENTS;
		QUEUE = TERATERM_QUEUE;
		FLAG_11[9] = 0;
	}
	else if (FLAG_11[6] == 1){
		base = UART3;
		EVENTS = BLUETOOTH_EVENTS;
		QUEUE = BLUETOOTH_QUEUE;
		FLAG_11[9] = 1;
	}


	for (;;) {
		CLEAR_SCREEN(base);
		UART_WriteBlocking(base, (void *) "\033[1;10H", sizeof("\033[1;10H"));
		UART_WriteBlocking(base, (void *) GET_FECHA, sizeof(GET_FECHA));

		RTC_SUB_ADDR = 0x00; //Comienza con las horas
		BANDERA_H_F = 1;

		//vTaskResume(RTC_READ_HANDLER);
		xTaskCreate(RTC_READ, "RTC_READ",configMINIMAL_STACK_SIZE,base, TASK_2_PRIORITY, &RTC_READ_HANDLER);

		xBits = xEventGroupWaitBits(EVENTS, ESC | ENT, pdTRUE, pdFALSE,
		portMAX_DELAY);
		xEventGroupClearBits(EVENTS, ESC | ENT | DONE);
		vTaskDelete(RTC_READ_HANDLER);
		vTaskResume(INIT_MENU_HANDLE);
	}
}

void TASK_8(void * pvParams) {
	EventBits_t xBits;
	uint8_t Counter;
	uint8_t ITEM_QUEUE;
	uint8_t BUFF[150];
	UART_Type *base = (UART_Type *) pvParams;
	UART_Type *base2;
	FLAG_11[9] = 0;
	if (UART0 == base) {
		base2 = UART3;
	} else if (UART3 == base) {
		base2 = UART0;
	}

	for (;;) {
//		CLEAR_SCREEN();

		UART_WriteBlocking(base, (void *) "\033[1;10H", sizeof("\033[1;10H"));
		UART_WriteBlocking(base, (void *) CHAT, sizeof(CHAT));
		//	xBits = xEventGroupWaitBits(Evento, ESC | ENT | CHAR, pdTRUE, pdFALSE, portMAX_DELAY);

		for (;;) {

			for (Counter = 0; Counter < 150; Counter++) {
				xBits = xEventGroupWaitBits(TERATERM_EVENTS, ESC | ENT | CHAR,
				pdTRUE, pdFALSE, portMAX_DELAY);
				if (ESC == (ESC & xBits))
					break;
				else if (ENT == (ENT & xBits))
					break;
			}

			if (ESC == (ESC & xBits))
				break;

//			  	/** Limpiamos el Enter o el ESZ que usamos para salir de la escritura del mensaje**/
			xEventGroupClearBits(TERATERM_EVENTS,
			ALL | CHAR | ESC | ENT | DONE);
//
//			  	/** C�lculamos la longuitud del mensaje */
			MENSAJE = uxQueueMessagesWaiting(TERATERM_QUEUE);
//
//			  	/** Guardamos lo que se escribio en un buffer que luego mandaremos a escribir en la memoria */
			for (Counter = 0; Counter < MENSAJE; Counter++) {

				xQueueReceive(TERATERM_QUEUE, &ITEM_QUEUE, portMAX_DELAY);
				//				UART_WriteBlocking(base2,(void *)(ITEM_QUEUE+0x30),sizeof(ITEM_QUEUE+0x30));
				BUFF[Counter] = ITEM_QUEUE;
			}

			UART_WriteBlocking(base2, (void *) BUFF, sizeof(BUFF));
//

		}
		xBits = xEventGroupWaitBits(TERATERM_EVENTS, ESC | ENT, pdTRUE, pdFALSE,
		portMAX_DELAY);
		xEventGroupClearBits(TERATERM_EVENTS, ESC | ENT | DONE);
		if (base == UART0)
			vTaskResume(INIT_MENU_HANDLE);
		if (base == UART3)
			vTaskResume(INIT_MENU_HANDLE);

	}
}
void TASK_8_BT(void * pvParams) {
	EventBits_t xBits;
	uint8_t Counter;
	uint8_t ITEM_QUEUE;
	uint8_t BUFF[150];
	UART_Type *base = (UART_Type *) pvParams;
	UART_Type *base2;
	FLAG_11[9] = 1;
	if (UART0 == base) {
		base2 = UART3;
	} else if (UART3 == base) {
		base2 = UART0;
	}

	for (;;) {
//		CLEAR_SCREEN();

		UART_WriteBlocking(base, (void *) "\033[1;10H", sizeof("\033[1;10H"));
		UART_WriteBlocking(base, (void *) CHAT, sizeof(CHAT));
		//	xBits = xEventGroupWaitBits(Evento, ESC | ENT | CHAR, pdTRUE, pdFALSE, portMAX_DELAY);

		for (;;) {

			for (Counter = 0; Counter < 150; Counter++) {
				xBits = xEventGroupWaitBits(BLUETOOTH_EVENTS, ESC | ENT | CHAR,
				pdTRUE, pdFALSE, portMAX_DELAY);
				if (ESC == (ESC & xBits))
					break;
				else if (ENT == (ENT & xBits))
					break;
			}

			if (ESC == (ESC & xBits))
				break;

//			  	/** Limpiamos el Enter o el ESZ que usamos para salir de la escritura del mensaje**/
			xEventGroupClearBits(BLUETOOTH_EVENTS,
			ALL | CHAR | ESC | ENT | DONE);
//
//			  	/** C�lculamos la longuitud del mensaje */
			MENSAJE = uxQueueMessagesWaiting(BLUETOOTH_QUEUE);
//
//			  	/** Guardamos lo que se escribio en un buffer que luego mandaremos a escribir en la memoria */
			for (Counter = 0; Counter < MENSAJE; Counter++) {

				xQueueReceive(BLUETOOTH_QUEUE, &ITEM_QUEUE, portMAX_DELAY);
				//				UART_WriteBlocking(base2,(void *)(ITEM_QUEUE+0x30),sizeof(ITEM_QUEUE+0x30));
				BUFF[Counter] = ITEM_QUEUE;
			}

			UART_WriteBlocking(base2, (void *) BUFF, sizeof(BUFF));
//

		}
		xBits = xEventGroupWaitBits(BLUETOOTH_EVENTS, ESC | ENT, pdTRUE,
		pdFALSE, portMAX_DELAY);
		xEventGroupClearBits(BLUETOOTH_EVENTS, ESC | ENT | DONE);
		if (base == UART0)
			vTaskResume(INIT_MENU_HANDLE);
		if (base == UART3)
			vTaskResume(INIT_MENU_HANDLE);

	}

}
void TASK_9(void * pvParameters) {
	uint8_t Counter;
	uint8_t caracter;
	EventBits_t xBits;
	UART_Type *base;
	EventGroupHandle_t *EVENTS;
	QueueHandle_t *QUEUE;
	if (FLAG_11[7] == 0){
		base = UART0;
		EVENTS = TERATERM_EVENTS;
		QUEUE = TERATERM_QUEUE;
		FLAG_11[9] = 0;
	}
	else if (FLAG_11[7] == 1){
		base = UART3;
		EVENTS = BLUETOOTH_EVENTS;
		QUEUE = BLUETOOTH_QUEUE;
		FLAG_11[9] = 1;
	}

	for (;;) {
		for (;;) {
			CLEAR_SCREEN(base);
			UART_WriteBlocking(base, (void *) "\033[1;10H",
					sizeof("\033[1;10H"));
			UART_WriteBlocking(base, (void *) ECO, sizeof(ECO));

			LCDNokia_init();
			LCDNokia_clear();
			LCDNokia_gotoXY(0x10, 0x10);

			for (Counter = 0; Counter < 150; Counter++) {
				xBits = xEventGroupWaitBits(EVENTS, ESC | ENT | CHAR,
				pdTRUE, pdFALSE, portMAX_DELAY);

				if (ESC == (ESC & xBits))
					break;
				else if (ENT == (ENT & xBits))
					break;
				xQueueReceive(QUEUE, &caracter, portMAX_DELAY);
				LCD_delay();
				LCDNokia_sendChar(caracter);
				LCD_delay();
			}
			if (ESC == (ESC & xBits))
				break;
		}
		vTaskResume(INIT_MENU_HANDLE);
	}
}

void UART0_RX_TX_IRQHandler(void) {

	while (!(UART0->S1 & UART_S1_RDRF_MASK))
		;
	TERATERM_Char = UART_ReadByte(UART0);

	if (ESCAPE == TERATERM_Char) {
		xEventGroupSetBitsFromISR(TERATERM_EVENTS, ESC, &xPriority);
		xEventGroupClearBitsFromISR(TERATERM_EVENTS, ALL | CHAR | ENT);
		UART_WriteBlocking(UART0, (void *) "[ESC]", sizeof("[ESC]"));
	} else if (ENTER == TERATERM_Char) {
		xEventGroupSetBitsFromISR(TERATERM_EVENTS, ENT, &xPriority);
		xEventGroupClearBitsFromISR(TERATERM_EVENTS, ALL | CHAR);
		UART_WriteBlocking(UART0, (void *) "[INT]", sizeof("[INT]"));
	} else {
		EventBits_t xBits;
		xBits = xEventGroupGetBitsFromISR(TERATERM_EVENTS);
		if ( ALL != (ALL & xBits)) {
			UART_WriteByte(UART0, TERATERM_Char);
			xQueueSendFromISR(TERATERM_QUEUE, &TERATERM_Char, &xPriority); // Post an item on a queue.
			xEventGroupSetBitsFromISR(TERATERM_EVENTS, CHAR, &xPriority);
		}
	}
}

void UART3_RX_TX_IRQHandler(void) {

	while (!(UART3->S1 & UART_S1_RDRF_MASK))
		;
	BLUETOOTH_Char = UART_ReadByte(UART3);

	if (ESCAPE == BLUETOOTH_Char) {
		xEventGroupSetBitsFromISR(BLUETOOTH_EVENTS, ESC, &xPriority);
		xEventGroupClearBitsFromISR(BLUETOOTH_EVENTS, ALL | CHAR | ENT);
		UART_WriteBlocking(UART3, (void *) "[ESC]", sizeof("[ESC]"));
	} else if (ENTER == BLUETOOTH_Char) {
		xEventGroupSetBitsFromISR(BLUETOOTH_EVENTS, ENT, &xPriority);
		xEventGroupClearBitsFromISR(BLUETOOTH_EVENTS, ALL | CHAR);
		UART_WriteBlocking(UART3, (void *) "[INT]", sizeof("[INT]"));
	} else {
		EventBits_t xBits;
		xBits = xEventGroupGetBitsFromISR(BLUETOOTH_EVENTS);
		if ( ALL != (ALL & xBits)) {
			UART_WriteByte(UART3, BLUETOOTH_Char);
			GPIO_delay(1000);
			xQueueSendFromISR(BLUETOOTH_QUEUE, &BLUETOOTH_Char, &xPriority); // Post an item on a queue.
			xEventGroupSetBitsFromISR(BLUETOOTH_EVENTS, CHAR, &xPriority);
		}
	}
}

