/*
 * LCDSPI.h
 *
 *  Created on: 23/04/2017
 *      Author: esteban
 */

#ifndef SOURCE_LCDSPI_H_
#define SOURCE_LCDSPI_H_


#include <string.h>
#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_port.h"
#include "fsl_dspi.h"
#include "fsl_gpio.h"

#define SPI_PINDC 			0x03
#define SPI_PINDIN			0x02
#define SPI_PINCLK			0x01
#define SPI_PINRST			0x00
#define LCD_CMD 			0
#define LCD_DATA 			1
#define DSPI0_CLK			CLOCK_GetFreq(kCLOCK_BusClk)

#define SCREENW 84
#define SCREENH 48

#define LCD_X 84
#define LCD_Y 48


dspi_master_config_t masterConfig;
dspi_command_data_config_t command;

/*It configures the LCD*/
void LCDNokia_init(void);
/*It writes a byte in the LCD memory. The place of writting is the last place that was indicated by LCDNokia_gotoXY. In the reset state
 * the initial place is x=0 y=0*/
void LCDNokia_writeByte(uint8_t, uint8_t);
/*it clears all the figures in the LCD*/
void LCDNokia_clear(void);
/*It is used to indicate the place for writing a new character in the LCD. The values that x can take are 0 to 84 and y can take values
 * from 0 to 5*/
void LCDNokia_gotoXY(uint8_t x, uint8_t y);
/*It allows to write a figure represented by constant array*/
void LCDNokia_bitmap(const uint8_t*);
/*It write a character in the LCD*/
void LCDNokia_sendChar(uint8_t);
/*It write a string into the LCD*/
void LCDNokia_sendString(uint8_t*);
/*It used in the initialization routine*/
void LCD_delay(void);
/*It used in the initialization routine*/
void delay(uint32_t);





#endif /* SOURCE_LCDSPI_H_ */
